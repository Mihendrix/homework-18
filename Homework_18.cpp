﻿#include <iostream> 
#include <windows.h>
#include <string> 



class Player
{
private:
    std::string Name{};
    int Score{};

public:
    Player() {}
    Player(std::string name, int score)
    {
        Name = name;
        Score = score;
    }

    std::string GetName()
    {
        return Name;
    }

    int GetScore()
    {
        return Score;
    }
};

void SetPlayersList(Player* playersList, int* playersAmount)
{
    using namespace std;
    Player* Players = playersList;
    for (int i = 0; i < *playersAmount; i++)
    {
        cout << "Введите имя " << i + 1 << "го игрока: ";
        string name;
        cin >> name;
        cout << "Введите количество очков " << i + 1 << "го игрока: ";
        int score;
        cin >> score;
        playersList[i] = Player(name, score);
    }
}

void PrintPlayersList(Player* playersList, int* playersAmount)
{
    using namespace std;
    cout << "\n";
    for (int i = 0; i < *playersAmount; i++)
    {
        Player player = playersList[i];
        cout << "Игрок " << i + 1 << ":\n";
        cout << "Имя: " << player.GetName() << "\n";
        cout << "Очки: " << player.GetScore() << "\n\n";
    }
}

void SortPlayersListByScore(Player* playersList, int* playersAmount)
{
    Player tempPlayer;
    for (int current = 0; current < *playersAmount; current++)
    {
        for (int next = current + 1; next < *playersAmount; next++)
        {
            tempPlayer = playersList[next];
            if (playersList[next].GetScore() < playersList[current].GetScore())
            {
                playersList[next] = playersList[current];
                playersList[current] = tempPlayer;
            }
        }
    } 
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    std::cout << "Введите количество игроков: ";
    int* PlayersAmount = new int{};
    std::cin >> *PlayersAmount;
    Player* PlayersList = new Player[*PlayersAmount];

    std::cout << "\nВведите имена игроков и количество очков: " << "\n\n";
    SetPlayersList(PlayersList, PlayersAmount);
    std::cin.ignore();
    PrintPlayersList(PlayersList, PlayersAmount);
    std::cin.ignore();
    SortPlayersListByScore(PlayersList, PlayersAmount);
    PrintPlayersList(PlayersList, PlayersAmount);
}

 
